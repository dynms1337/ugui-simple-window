﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using System.Collections.Generic;
using uGUISimpleWindow.core;

namespace uGUISimpleWindow
{
    public enum LayoutType
    {
        Grid,
        Horizontal,
        Vertical,
        None,
    }

    [Serializable]
    public class UIInfo
    {
        public enum Fit { Parent, WParentHSelf, Self, Fixed, Flexible, WParentHFrexible, UnSpecified }

        public UIInfo() { }

        public Fit m_fit = Fit.UnSpecified;
        public Vector2 m_margin = Vector2.zero;   //Parentで使用
        public Vector2 m_position = Vector2.zero;       //SpecifiedSizeで使用
        public Vector2 m_uiSize = Vector2.zero;           //SpecifiedSizeで使用、０なら設定しない
        public float m_flexWidth = 0f;        //SpecifiedSizeで使用、０なら設定しない
        public float m_flexHeight = 0f;       //SpecifiedSizeで使用、０なら設定しない
        public int m_textSize = 14;
        public Color m_textColor = SWHelper.COLOR_TEXT;
        public Color m_bgColor = default(Color);      //デフォルト値なら、各UI要素作成時に適切な色を選択
        public TextAnchor m_textAlignment = TextAnchor.MiddleLeft;


        /* フィールド編集用メソッド
         * メソッドチェインでの利用を想定
         * Usage:
         *  UIInfo uiInfo1 = new UIInfo().fit_Parent().bgColor(Color.red);
         *  UIInfo uiInfo2 = uiInfo1.textSize(14);
         *  
         *  ※　呼び出し元のUIInfoの変更を避けるため、クローンを編集して返却している。
        */
        public UIInfo margin(Vector2 margin) { UIInfo ret = this.Clone(); ret.m_margin = margin; return ret; }
        public UIInfo position(Vector2 position) { UIInfo ret = this.Clone(); ret.m_position = position; return ret; }
        public UIInfo uiSize(Vector2 uiSize) { UIInfo ret = this.Clone(); ret.m_uiSize = uiSize; return ret; }
        public UIInfo flexWidth(float flexWidth) { UIInfo ret = this.Clone(); ret.m_flexWidth = flexWidth; return ret; }
        public UIInfo flexHeight(float flexHeight) { UIInfo ret = this.Clone(); ret.m_flexHeight = flexHeight; return ret; }
        public UIInfo textSize(int sizePx) { UIInfo ret = this.Clone(); ret.m_textSize = sizePx; return ret; }
        public UIInfo textColor(Color textColor) { UIInfo ret = this.Clone(); ret.m_textColor = textColor; return ret; }
        public UIInfo bgColor(Color bgColor) { UIInfo ret = this.Clone(); ret.m_bgColor = bgColor; return ret; }
        public UIInfo textAlignment(TextAnchor textAlignment) { UIInfo ret = this.Clone(); ret.m_textAlignment = textAlignment; return ret; }

        /// <summary>縦横固定サイズ[px]</summary>
        public UIInfo fit_Fixed(Vector2 position = default(Vector2), Vector2 uiSize = default(Vector2))
        {
            UIInfo ret = this.Clone(); ret.m_fit = Fit.Fixed;
            if (position != default(Vector2)) ret.m_position = position;
            if (uiSize != default(Vector2)) ret.m_uiSize = uiSize;
            return ret;
        }
        /// <summary>親GameObjectのサイズに合わせる。</summary>
        public UIInfo fit_Parent(Vector2 margin = default(Vector2))
        {
            UIInfo ret = this.Clone(); ret.m_fit = Fit.Parent;
            if (margin != default(Vector2)) ret.m_margin = margin;
            return ret;
        }
        /// <summary>自身のコンポーネントのサイズに合わせる</summary>
        public UIInfo fit_Self()
        {
            UIInfo ret = this.Clone(); ret.m_fit = Fit.Self;
            return ret;
        }
        /// <summary>親GameObjectに隙間ができないように合わせる</summary>
        public UIInfo fit_Flexible(float flexWidth = 1f, float flexHeight = 1f, Vector2 minSize = default(Vector2))
        {
            if (minSize == default(Vector2)) minSize = Vector2.zero;
            UIInfo ret = this.Clone(); ret.m_fit = Fit.Flexible;
            ret.m_flexWidth = flexWidth; ret.m_flexHeight = flexHeight; ret.m_uiSize = minSize;
            return ret;
        }
        /// <summary>横幅は親、縦幅は自身のサイズに合わせる</summary>
        public UIInfo fit_WParentHSelf(Vector2 margin = default(Vector2))
        {
            UIInfo ret = this.Clone(); ret.m_fit = Fit.WParentHSelf;
            if (margin != default(Vector2)) ret.m_margin = margin;
            return ret;
        }

        public UIInfo Clone()     // ディープコピー
        {
            string jsonStr = JsonUtility.ToJson(this);
            return JsonUtility.FromJson<UIInfo>(jsonStr);
        }

        public bool is_fit_UnSpecified() { return this.m_fit == Fit.UnSpecified; }
    }

    public class SPanel
    {
        const int DEFAULT_GRIDPANEL_CELLWIDTH = 100;
        public readonly GameObject panel;

        private SPanel(LayoutGroup layoutGroup) { this.panel = layoutGroup.gameObject; }

        /// <summary>
        /// GameObject `parent`の子GameObjectを作成して、VerticalLayoutGroupコンポーネントを追加する。
        /// 戻り値のSPanelオブジェクトを通して、UI要素を追加できる。
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="uiInfo"></param>
        /// <returns></returns>
        public static SPanel CreateVertical(GameObject parent, UIInfo uiInfo = null)
        { return new SPanel(SWHelper.CreatePanel(uiInfo, layoutGroup: LayoutType.Vertical, parent: parent)); }
        /// <summary>
        /// GameObject `parent`の子GameObjectを作成して、HorizontalLayoutGroupコンポーネントを追加する。
        /// 戻り値のSPanelオブジェクトを通して、UI要素を追加できる。
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="uiInfo"></param>
        /// <returns></returns>
        public static SPanel CreateHorizontal(GameObject parent, UIInfo uiInfo = null)
        { return new SPanel(SWHelper.CreatePanel(uiInfo, layoutGroup: LayoutType.Horizontal, parent: parent)); }
        /// <summary>
        /// GameObject `parent`の子GameObjectを作成して、GridLayoutGroupコンポーネントを追加する。
        /// 戻り値のSPanelオブジェクトを通して、UI要素を追加できる。
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="cellWidthPx"></param>
        /// <param name="aspectRatio"></param>
        /// <param name="uiInfo"></param>
        /// <returns></returns>
        public static SPanel CreateGrid(GameObject parent, int cellWidthPx = -1, float aspectRatio = 1, UIInfo uiInfo = null)
        {
            if (uiInfo == null) uiInfo = new UIInfo();
            GridLayoutGroup lg = (GridLayoutGroup)SWHelper.CreatePanel(uiInfo.fit_WParentHSelf(), layoutGroup: LayoutType.Grid, parent: parent);
            float width = DEFAULT_GRIDPANEL_CELLWIDTH;
            if (cellWidthPx >= 1) { width = cellWidthPx; }
            lg.cellSize = new Vector2(width, width * aspectRatio);
            return new SPanel(lg);
        }
        /// <summary>
        /// `layoutGroup`コンポーネントから、SPanelを作成する。
        /// </summary>
        /// <param name="layoutGroup"></param>
        /// <returns></returns>
        public static SPanel CreateFromPanel(LayoutGroup layoutGroup) { return new SPanel(layoutGroup); }

        /// <summary>
        /// VerticalLayoutGroupコンポーネントを持ったGameObjectを子要素に追加して、LayoutGroupに対応するSPanelを返す
        /// </summary>
        /// <param name="uiInfo"></param>
        /// <returns></returns>
        public SPanel addPanel_Vertical(UIInfo uiInfo = null)
        {
            if (uiInfo == null) uiInfo = new UIInfo();
            if (uiInfo.is_fit_UnSpecified()) uiInfo = uiInfo.fit_WParentHSelf();
            return CreateVertical(panel, uiInfo);
        }
        /// <summary>
        /// HorizontalLayoutGroupコンポーネントを持ったGameObjectを子要素に追加して、LayoutGroupに対応するSPanelを返す
        /// </summary>
        /// <param name="uiInfo"></param>
        /// <returns></returns>
        public SPanel addPanel_Horizontal(UIInfo uiInfo = null) { return CreateHorizontal(panel, uiInfo); }
        /// <summary>
        /// GridLayoutGroupコンポーネントを持ったGameObjectを子要素に追加して、LayoutGroupに対応するSPanelを返す
        /// </summary>
        /// <param name="cellWidthPx">GridのCellの横幅のピクセル数</param>
        /// <param name="aspectRatio">Cellのアスペクト比</param>
        /// <param name="uiInfo"></param>
        /// <returns></returns>
        public SPanel addPanel_Grid(int cellWidthPx = -1, float aspectRatio = 1, UIInfo uiInfo = null)
        { return CreateGrid(panel, cellWidthPx, aspectRatio, uiInfo); }
        /// <summary>
        /// Textコンポーネントを持ったGameObjectを子要素に追加して、Textコンポーネントを返す
        /// </summary>
        /// <param name="label">Textに表示する文字列</param>
        /// <param name="uiInfo"></param>
        /// <returns></returns>
        public Text addText(string label, UIInfo uiInfo = null)
        { return SWHelper.CreateText(panel, label, uiInfo: uiInfo); }
        /// <summary>
        /// 空白を表現するために空のImageコンポーネントを持ったGameObjectを子要素に追加して、Imageコンポーネントを返す
        /// </summary>
        /// <param name="height">空白の高さのピクセル数</param>
        /// <returns></returns>
        public Image addSpacer(int height = 0) { return SWHelper.CreateSpacer(panel, height); }
        /// <summary>
        /// 水平方向の罫線を表現するためにImageコンポーネントを持ったGameObjectを子要素に追加して、Imageコンポーネントを返す
        /// </summary>
        /// <param name="height">罫線の高さ（太さ）のピクセル数</param>
        /// <param name="color">罫線の色</param>
        /// <returns></returns>
        public Image addHorizontalBar(int height = SWHelper.HR_HEIGHT, Color color = default(Color)) { return SWHelper.CreateHorizontalBar(panel, height, color); }
        /// <summary>
        /// Buttonコンポーネントを持ったGameObjectを子要素に追加して、Buttonコンポーネントを返す
        /// </summary>
        /// <param name="TaskOnClick">ボタンをクリックしたときの動作</param>
        /// <param name="labelStr">ボタンに表示する文字列</param>
        /// <param name="texPath">ボタンに表示する画像の、ファイルへのパス。（画像を表示するときは、texPathかtexのどちらかを指定する）</param>
        /// <param name="tex">ボタンに表示する画像。（画像を表示するときは、texPathかtexのどちらかを指定する）</param>
        /// <param name="uiInfo"></param>
        /// <returns></returns>
        public Button addButton(UnityAction TaskOnClick, string labelStr = "", string texPath = "", Texture2D tex = null, UIInfo uiInfo = null)
        {
            // labelStrとTexture2Dオブジェクトからボタンを作る場合
            if (texPath == "") return SWHelper.CreateButton(panel, TaskOnClick: TaskOnClick, labelStr: labelStr, tex: tex, uiInfo: uiInfo);
            // texPathからTexture2Dを読込んでボタンを作る場合
            else return SWHelper.CreateButton(panel, texPath: texPath, TaskOnClick: TaskOnClick, labelStr: labelStr, uiInfo: uiInfo);
        }
        /// <summary>
        /// ラジオボタンを表示するために、ToggleGroupコンポーネントを持ったGameObjectを子要素に追加して、ToggleGroupコンポーネントを返す。
        /// ラジオボタンを選択すると、`showValueDict`で指定した値を引数にして`onValueChanged`が実行される。
        /// </summary>
        /// <typeparam name="T">onValueChangedの引数の型</typeparam>
        /// <param name="onValueChanged">ラジオボタンを選択したときに実行する処理。</param>
        /// <param name="showValueDict">キーがラジオボタンの表示文字列、値が`onValueChanged`に渡される値　の辞書。</param>
        /// <param name="selected">初期状態で選択されているラジオボタンの番号</param>
        /// <param name="layoutGroup">ラジオボタンの配置方法</param>
        /// <returns></returns>
        public ToggleGroup addRadioButton<T>(UnityAction<T> onValueChanged, Dictionary<string, T> showValueDict, int selected = 0,
            LayoutType layoutGroup = LayoutType.Horizontal)
        { return SWHelper.CreateRadioButton(panel, onValueChanged, showValueDict, selected, layoutGroup); }
        /// <summary>
        /// Sliderコンポーネントを持ったGameObjectを子要素に追加して、Sliderコンポーネントを返す
        /// </summary>
        /// <param name="onValueChanged">Sliderを動かしたときに実行する処理</param>
        /// <returns></returns>
        public Slider addSlider(UnityAction<float> onValueChanged) { return SWHelper.CreateSlider(panel, onValueChanged); }
        /// <summary>
        /// Toggleコンポーネントを持ったGameObjectを子要素に追加して、Toggleコンポーネントを返す
        /// </summary>
        /// <param name="onValueChanged">Toggleを選択したときに実行する処理</param>
        /// <param name="labelStr">Toggleに表示する文字列</param>
        /// <param name="isOn">ToggleのON/OFFの初期状態</param>
        /// <returns></returns>
        public Toggle addToggle(UnityAction<bool> onValueChanged, string labelStr, bool isOn = true)
        { return SWHelper.CreateToggle(panel, onValueChanged, labelStr, isOn: isOn); }
        /// <summary>
        /// Imageコンポーネントを持ったGameObjectを子要素に追加して、Imageコンポーネントを返す
        /// </summary>
        /// <param name="path">ボタンに表示する画像の、ファイルへのパス</param>
        /// <param name="size">画像をリスケールするときのピクセル数</param>
        /// <param name="color">指定した色を画像に対して乗算する。デフォルト値（Color.white）なら元の色で表示される。</param>
        /// <returns></returns>
        public Image addImage(string path, Vector2 size = default(Vector2), Color color = default(Color))
        {
            UIInfo uiInfo = new UIInfo().fit_Self().bgColor(color);
            if (size != default(Vector2)) uiInfo = uiInfo.fit_Fixed(Vector2.zero, size);
            return SWHelper.CreateImage(panel, path, width: (int)size.x, height: (int)size.y, uiInfo: uiInfo);
        }
        /// <summary>
        /// Imageコンポーネントを持ったGameObjectを子要素に追加して、Imageコンポーネントを返す
        /// </summary>
        /// <param name="sprite">ボタンに表示する画像</param>
        /// <param name="size">画像をリスケールするときのピクセル数</param>
        /// <param name="color">指定した色を画像に対して乗算する。デフォルト値（Color.white）なら元の色で表示される。</param>
        /// <returns></returns>
        public Image addImage(Sprite sprite = null, Vector2 size = default(Vector2), Color color = default(Color))
        {
            UIInfo uiInfo = new UIInfo().fit_Self().bgColor(color);
            if (size != default(Vector2)) uiInfo = uiInfo.fit_Fixed(Vector2.zero, size);
            return SWHelper.CreateImage(panel, sprite, uiInfo: uiInfo);
        }
        /// <summary>
        /// TextFieldコンポーネントを持ったGameObjectを子要素に追加して、TextFieldコンポーネントを返す
        /// </summary>
        /// <param name="onEndEdit">文字列の編集を確定したときに実行する処理</param>
        /// <param name="onValueChanged">文字列を編集したときに実行する処理</param>
        /// <param name="defaultText">表示する文字列の初期値</param>
        /// <param name="uiInfo"></param>
        /// <param name="lineCount">文字列の編集領域の行数</param>
        /// <returns></returns>
        public InputField addTextField(UnityAction<string> onEndEdit = null, UnityAction<string> onValueChanged = null,
            string defaultText = "", UIInfo uiInfo = null, int lineCount = 1)
        { return SWHelper.CreateInputField(panel, onEndEdit, onValueChanged, defaultText, lineCount, uiInfo); }


    }
    public class SWindow
    {
        private readonly SPanel container;
        public readonly SPanel header;
        public readonly SPanel caption;
        public readonly SPanel content;
        public readonly SPanel hooter;


        private SWindow(LayoutGroup lgContainer, string title = "", LayoutType headerLayout = LayoutType.Horizontal, LayoutType hooterLayout = LayoutType.Horizontal,
            UIInfo uiInfo = null, GameObject parent = null)
        {
            if (uiInfo == null) uiInfo = new UIInfo();
            this.container = SPanel.CreateFromPanel(lgContainer);

            SPanel _header_parent = SPanel.CreateFromPanel(SWHelper.CreatePanel(uiInfo: new UIInfo().fit_WParentHSelf().bgColor(uiInfo.m_bgColor),
                layoutGroup: LayoutType.Horizontal, parent: container.panel, goName: title + "-headerContainer"));
            this.header = SPanel.CreateFromPanel(SWHelper.CreatePanel(uiInfo: new UIInfo().fit_WParentHSelf().bgColor(uiInfo.m_bgColor),
                layoutGroup: LayoutType.Horizontal, parent: _header_parent.panel, "left"));  //子要素のサイズに合わせる
            this.caption = SPanel.CreateFromPanel(SWHelper.CreatePanel(uiInfo: new UIInfo().fit_Self().bgColor(uiInfo.m_bgColor),
                layoutGroup: LayoutType.Horizontal, parent: _header_parent.panel, "right")); //子要素のサイズに合わせる
            this.header.addText(title);

            this.content = SPanel.CreateFromPanel(
                SWHelper.CreateScrollView(container.panel, uiInfo: uiInfo.fit_Flexible(), goName: title + "-content"));
            this.hooter = SPanel.CreateFromPanel(
                SWHelper.CreatePanel(uiInfo: uiInfo, layoutGroup: hooterLayout, parent: container.panel, goName: title + "-hooter"));
        }

        /// <summary>
        /// ウィンドウを作成して、スクリーン上にUIを配置する。（CanvasのRenderModeがScreenSpaceOverlayになる）
        /// </summary>
        /// <param name="title">ウィンドウのタイトル</param>
        /// <param name="leftbottom">ウィンドウの左下角の座標（原点：スクリーンの左下、右・上方向が正）</param>
        /// <param name="windowSize">ウィンドウのサイズ</param>
        /// <param name="headerLayout">ヘッダーパネル内の要素の配置方法（デフォルト：横に配置）</param>
        /// <param name="hooterLayout">フッターパネル内の要素の配置方法（デフォルト：横に配置）</param>
        /// <param name="uiInfo">UI要素のプロパティの指定（背景色とか）</param>
        /// <param name="draggable">ウィンドウをドラッグ可能にするか</param>
        /// <param name="canvasScale">UI要素のサイズを微調整。文字サイズのバランスやUIの配置を保ったままウィンドウを大きくしたいときに使う。</param>
        /// <param name="parent">親GameObject</param>
        /// <returns></returns>
        public static SWindow Create_onScreen(string title = "", Vector2 leftbottom = default(Vector2), Vector2 windowSize = default(Vector2),
            LayoutType headerLayout = LayoutType.Horizontal, LayoutType hooterLayout = LayoutType.Horizontal, UIInfo uiInfo = null, bool draggable = true,
             float canvasScale = 1f, GameObject parent = null)
        {
            LayoutGroup lg = SWHelper.CreateWindowWithCanvas_onScreen(leftbottom, windowSize, uiInfo: null, layoutGroup: LayoutType.Vertical,
                draggable: draggable, canvasScale: canvasScale, parent: parent, goName: title + "-window");
            return new SWindow(lg, title, headerLayout, hooterLayout, uiInfo, parent);
        }

        /// <summary>
        /// ウィンドウを作成して、ゲーム空間にUIを配置する。（CanvasのRenderModeがWorldSpaceになる）
        /// </summary>
        /// <param name="title">ウィンドウのタイトル</param>
        /// <param name="leftbottom">ウィンドウの左下角の座標（原点：スクリーンの左下、右・上方向が正）</param>
        /// <param name="windowSize">ウィンドウのサイズ</param>
        /// <param name="headerLayout">ヘッダーパネル内の要素の配置方法（デフォルト：横に配置）</param>
        /// <param name="hooterLayout">フッターパネル内の要素の配置方法（デフォルト：横に配置）</param>
        /// <param name="uiInfo">UI要素のプロパティの指定（背景色とか）</param>
        /// <param name="camera">どのカメラを通してUIを操作するかを指定</param>
        /// <param name="meterPerPx">１ピクセルをゲーム空間の何m（or 距離単位）に割り当てるか</param>
        /// <param name="windowScale">ウィンドウ全体のサイズを微調整。文字サイズのバランスやUIの配置を保ったままウィンドウを大きくしたいときに使う。</param>
        /// <param name="parent">親GameObject</param>
        /// <returns></returns>
        public static SWindow Create_onWorld(string title = "", Vector2 leftbottom = default(Vector2), Vector2 windowSize = default(Vector2),
            LayoutType headerLayout = LayoutType.Horizontal, LayoutType hooterLayout = LayoutType.Horizontal, UIInfo uiInfo = null,
             Camera camera = null, float meterPerPx = 0.001f, float windowScale = 1f, GameObject parent = null)
        {
            LayoutGroup lg = SWHelper.CreateWindowWithCanvas_onWorld(leftbottom, windowSize, uiInfo: null, layoutGroup: LayoutType.Vertical,
                  parent: parent, camera: camera, meterPerPx: meterPerPx, windowScale: windowScale, goName: title + "-window");
            return new SWindow(lg, title, headerLayout, hooterLayout, uiInfo, parent: parent);
        }

        /// <summary>
        /// キャプションボタン（ウィンドウ右上のボタン）を追加
        /// </summary>
        /// <param name="TaskOnClick"></param>
        /// <param name="labelStr"></param>
        /// <param name="texPath"></param>
        /// <param name="tex"></param>
        /// <param name="uiInfo"></param>
        /// <returns></returns>
        public Button addCaptionButton(UnityAction TaskOnClick, string labelStr = "", string texPath = "", Texture2D tex = null, UIInfo uiInfo = null)
        {
            Button ret = caption.addButton(TaskOnClick, labelStr, texPath, tex, uiInfo);
            _disable_flexWidth(ret.gameObject);  //すべてのGameObjectのflexWidthを0に設定
            return ret;
        }
        private void _disable_flexWidth(GameObject go)
        {
            LayoutElement le = go.GetComponent<LayoutElement>();
            if (le != null) le.flexibleWidth = 0f;
            SWHelper.foreach_GameObjectChildren(go, _disable_flexWidth);
        }

        /// <summary>
        /// ウィンドウの位置・サイズを、座標で指定
        /// （ウィンドウをスクリーン上に配置した場合のみ、正常に実行できる）
        /// </summary>
        /// <param name="left">ウィンドウ左下角の点のX座標</param>
        /// <param name="bottom">ウィンドウ左下角の点のY座標</param>
        /// <param name="width">ウィンドウの幅</param>
        /// <param name="height">ウィンドウの高さ</param>
        public void locate_byPosition(int left, int bottom, int width, int height)
        {
            RectTransform rt = container.panel.GetComponent<RectTransform>();
            rt.anchorMin = Vector2.zero; rt.anchorMax = Vector2.zero;
             rt.sizeDelta = new Vector2(width, height);
            //rt.localPosition = new Vector2(left, bottom);
            rt.position= new Vector2(left, bottom);
        }
        /// <summary>
        /// ウィンドウの位置・サイズを、スクリーン外周からの割合（PCT:PerCenTage）で指定
        /// （ウィンドウをスクリーン上に配置した場合のみ、正常に実行できる）
        /// </summary>
        /// <param name="left">スクリーン左端からの距離の割合</param>
        /// <param name="right">スクリーン右端からの距離の割合</param>
        /// <param name="top">スクリーン上端からの距離の割合</param>
        /// <param name="bottom">スクリーン下端からの距離の割合</param>
        public void locate_byMarginPct(float left, float right, float top, float bottom)
        {
            RectTransform rt = container.panel.GetComponent<RectTransform>();
            rt.anchorMin = new Vector2(left, bottom); rt.anchorMax = new Vector2(1 - right, 1 - top);
            rt.offsetMin = Vector2.zero; rt.offsetMax = Vector2.zero;
        }
        /// <summary>
        /// ウィンドウの位置・サイズを、スクリーン外周からのピクセル数で指定
        /// （ウィンドウをスクリーン上に配置した場合のみ、正常に実行できる）
        /// </summary>
        /// <param name="left">スクリーン左端からのピクセル数</param>
        /// <param name="right">スクリーン右端からのピクセル数</param>
        /// <param name="top">スクリーン上端からのピクセル数</param>
        /// <param name="bottom">スクリーン下端からのピクセル数</param>
        public void locate_byMarginPx(int left, int right, int top, int bottom)
        {
            RectTransform rt = container.panel.GetComponent<RectTransform>();
            rt.anchorMin = Vector2.zero; rt.anchorMax = Vector2.one;
            rt.offsetMin = new Vector2(left, bottom); rt.offsetMax = new Vector2(-right, -top);
        }
    }
}
