﻿using uGUISimpleWindow;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Example01 : MonoBehaviour
{
    void Start()
    {
        example01();
        example02();
    }

    private void example01()
    {
        SWindow window = SWindow.Create_onScreen("Example01", leftbottom: new Vector2(20, 20), windowSize: new Vector2(400, 800), hooterLayout: LayoutType.Vertical,canvasScale:1.2f);
        SPanel content = window.content;
        SPanel hooter = window.hooter;

        //キャプションボタンの追加
        window.addCaptionButton(() => { window.locate_byPosition(left: 10, bottom: 10, width: 300, height: 200); }, "-");  //最小化
        window.addCaptionButton(() => { window.locate_byMarginPct(left: 0.01f, right: 0.6f, top: 0.2f, bottom: 0.2f); }, "△");  //左に配置
        window.addCaptionButton(() => { window.locate_byMarginPx(left: 10, right: 20, top: 30, bottom: 40); }, "□");  //最大化
        window.addCaptionButton(() => { Debug.Log("Close Window Not Implemented"); }, labelStr: "×");

        //タイトル文字用のUI情報を作成
        UIInfo ui_titile = new UIInfo().textSize(16).textAlignment(TextAnchor.MiddleCenter).bgColor(new Color(0.1f, 0.2f, 0.5f, 0.5f));

        string s_hp = "HorizontalPanel追加の例";
        SPanel panel_hr = content.addPanel_Vertical();
        panel_hr.addText(s_hp, ui_titile);
        Dictionary<string, string> itemDict1 = new Dictionary<string, string>() { { "name0-0", "value0-0" }, { "name0-1", "value0-1" } };
        //Dictionary<string, string> itemDict2 = new Dictionary<string, string>() { { "name1-0", "value1-0" }, { "name1-1", "value0-1" } };
        foreach (var kvp in itemDict1)
        {
            SPanel hp = panel_hr.addPanel_Horizontal();
            hp.addText(kvp.Key);
            hp.addText(kvp.Value);
        }
        content.addHorizontalBar();
        content.addSpacer(5);

        string s_toggle = "トグルボタンのbool取得の例";
        SPanel panel_toggle = content.addPanel_Vertical();
        panel_toggle.addText(s_toggle, ui_titile);
        Text text3 = panel_toggle.addText("click toggle...");
        panel_toggle.addToggle(b => text3.text = "Toggle Status :" + (b ? "ON" : "OFF"), "switching", isOn: false);
        content.addHorizontalBar();
        content.addSpacer(5);

        string s_radio = "ラジオボタンから選択アイテム取得の例";
        SPanel panel_radio = content.addPanel_Vertical();
        panel_radio.addText(s_radio, ui_titile);
        Text text_radio = panel_radio.addText("click radio button...");
        panel_radio.addRadioButton(s => text_radio.text = "selected :" + s, itemDict1, layoutGroup: LayoutType.Horizontal);
        content.addHorizontalBar();
        content.addSpacer(5);

        string s_textfield = "１行のテキストボックスの例";
        SPanel panel_textfield = content.addPanel_Vertical();
        panel_textfield.addText(s_textfield, ui_titile);
        Text text1 = panel_textfield.addText("input text in text field...");
        panel_textfield.addTextField(onEndEdit: s => text1.text += "entered", onValueChanged: s => text1.text = "text field :" + s);
        content.addHorizontalBar();
        content.addSpacer(5);

        string s_textarea = "N行のテキストボックスの変更・確定検知の例";
        SPanel panel_textarea = content.addPanel_Vertical();
        panel_textarea.addText(s_textarea, ui_titile);
        Text text2 = panel_textarea.addText("input text...");
        panel_textarea.addTextField(onEndEdit: s => text2.text += "entered", onValueChanged: s => text2.text = "text area :" + s, lineCount:3);
        content.addHorizontalBar();
        content.addSpacer(5);

        string s_image = "画像追加の例";
        SPanel panel_image_container = content.addPanel_Vertical();
        panel_image_container.addText(s_image, ui_titile);
        SPanel panel_image = panel_image_container.addPanel_Horizontal();
        string imgPath = Application.dataPath + "/hot-chocolate.png";
        panel_image.addImage(imgPath);
        panel_image.addImage(imgPath, new Vector2(32, 32));
        panel_image.addButton(() => Debug.Log("Hot Chocolate Not Implimented"), "画像付きボタン", imgPath,
            uiInfo: new UIInfo().fit_Self().uiSize(new Vector2(64, 64)));
        content.addHorizontalBar();
        content.addSpacer(5);

        string s_grid = "グリッド追加の例";
        SPanel panel_grid_container = content.addPanel_Vertical(new UIInfo().bgColor(new Color(1f, 0f, 0f, 0.1f)));
        panel_grid_container.addText(s_grid, ui_titile);
        SPanel panel_grid = panel_grid_container.addPanel_Grid(cellWidthPx: 90, aspectRatio: 0.7f);
        panel_grid.addButton(() => Debug.Log("Hot Chocolate Not Implimented"), "画像付きボタン", imgPath);
        for (int i = 0; i < 4; i++) { panel_grid.addButton(() => Debug.Log("grid " + i + "th button clicked"), "button " + i); }
        for (int i = 0; i < 3; i++) { panel_grid.addText("Text " + i); }
        panel_grid.addTextField();
        panel_grid.addSlider(f => Debug.Log("slider : " + f));
        panel_grid.addToggle(b => Debug.Log("Toggle Status :" + (b ? "ON" : "OFF")), "switching", isOn: false);
        panel_grid.addRadioButton(s => Debug.Log("selected :" + s), itemDict1, layoutGroup: LayoutType.Vertical);
        content.addHorizontalBar();
        content.addSpacer(5);

        string s_slider = "スライダからfloat取得の例";
        SPanel panel_slider = hooter.addPanel_Vertical(new UIInfo().bgColor(new Color(0f, 1f, 0f, 0.1f)));
        panel_slider.addText(s_slider, ui_titile);
        Text text_slider = panel_slider.addText("drag slider...");
        panel_slider.addSlider(f => text_slider.text = "value :" + f.ToString());
        hooter.addHorizontalBar();
        hooter.addSpacer(5);
    }

    private void example02()
    {
        SWindow window = SWindow.Create_onWorld("Example02", parent: null, leftbottom: new Vector2(440, 100), windowSize: new Vector2(200, 600), meterPerPx:0.001f,windowScale:1.5f);
        SPanel caption = window.caption;
        SPanel content = window.content;

        content.addImage(size: new Vector2(200, 200), color: Color.red);
        content.addImage(size: new Vector2(200, 200), color: Color.green);
        content.addImage(size: new Vector2(200, 200), color: Color.blue);
        content.addImage(size: new Vector2(200, 200), color: Color.yellow);
    }
}

