﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using System.Collections.Generic;
using uGUISimpleWindow;
using System.Reflection;
using System.IO;
using Assets.core;

namespace uGUISimpleWindow.core
{
    public static class SWHelper
    {
        internal const string UI_LAYER_NAME = "UI";
        internal static int LAYOUTGROUP_SPACING = 1; //パネル内にxピクセル間隔で配置
        internal const int HR_HEIGHT = 1;
        internal const float SLIDER_MIN_WIDTH = 30f;
        internal const float SLIDER_MIN_HEIGHT = 10f;
        internal const float TOGGLE_BACKGROUND_SIZE = 20f;
        internal static Vector2 UIELEMENT_SIZE = new Vector2(160f, 20f);
        internal static Vector2 WINDOW_POSITION = new Vector2(10, 10);
        internal static Vector2 WINDOW_SIZE = new Vector2(400, 600);
        internal static Vector2 INPUTFIELD_MIN_SIZE = new Vector2(100, 30);
        internal static Color COLOR_TEXT = new Color(1f, 1f, 1f, 1f);
        internal static Color COLOR_AREA_BG = new Color(0.1f, 0.1f, 0.1f, 0.1f);
        internal static Color COLOR_SLIDER_HANDLE = new Color(0.8F, 0.8f, 0.8f, 1f);
        internal static Color COLOR_SLIDER_BACKGROUND = new Color(0F, 0f, 0f, 0.5f);
        internal static Color COLOR_SELECTABLE = new Color(0.6F, 0.6f, 0.6f, 1f);    //編集領域　テキストボックスetc
        internal static Color COLOR_CHECKMARK = new Color(0.8F, 0.8f, 0.8f, 1f);
        internal static Color COLOR_SCROLLVIEW_MASK = new Color(0.8F, 0.8f, 0.8f, 0.5f);
        internal static Color COLOR_SELECTABLE_NORMAL = new Color(0.6F, 0.6f, 0.6f, 1f);
        internal static Color COLOR_SELECTABLE_HOVER = new Color(0.7F, 0.7f, 0.7f, 1f);
        internal static Color COLOR_SELECTABLE_SELECTED = new Color(0.6F, 0.6f, 0.6f, 1f);
        internal static Color COLOR_SELECTABLE_PRESSED = new Color(1F, 1f, 1f, 1f);
        internal static Color COLOR_SELECTABLE_ON = new Color(1f, 1f, 1f, 1f);
        internal static Color COLOR_SELECTABLE_DISABLED = new Color(0.0F, 0.0f, 0.0f, 0.5f);
        internal static Color COLOR_IMAGE_DEFAULT = new Color(1F, 1f, 1f, 0.5f);

        internal static Font TEXT_FONT;
        private static UniqueName goname_Text = new UniqueName("Text");
        private static UniqueName goname_Button = new UniqueName("Button");
        private static UniqueName goname_Image = new UniqueName("Image");
        private static UniqueName goname_RawImage = new UniqueName("RawImage");
        private static UniqueName goname_Panel = new UniqueName("Panel");
        private static UniqueName goname_Window = new UniqueName("Window");
        private static UniqueName goname_Canvas = new UniqueName("Canvas");
        private static UniqueName goname_InputField = new UniqueName("InputField");
        private static UniqueName goname_Slider = new UniqueName("Slider");
        private static UniqueName goname_ScrollBar = new UniqueName("ScrollBar");
        private static UniqueName goname_ScrollView = new UniqueName("ScrollView");
        private static UniqueName goname_Toggle = new UniqueName("Toggle");
        private static UniqueName goname_RadioButton = new UniqueName("RadioButton");

        static SWHelper()
        {
            TEXT_FONT = UnityEngine.Resources.GetBuiltinResource<Font>("Arial.ttf");
        }

        private static GameObject CreateUIElement(string goName, UIInfo uiInfo, Vector2 sizeDelta = default(Vector2), GameObject parent = null)
        {
            if (sizeDelta == default(Vector2)) sizeDelta = UIELEMENT_SIZE;
            GameObject ret = new GameObject(goName);
            RectTransform rectTransform = ret.AddComponent<RectTransform>();
            rectTransform.sizeDelta = sizeDelta;
            {
                switch (uiInfo.m_fit)
                {
                    case UIInfo.Fit.Parent:
                        rectTransform.anchorMin = Vector2.zero;
                        rectTransform.anchorMax = Vector2.one;
                        rectTransform.anchoredPosition = Vector2.zero;
                        rectTransform.sizeDelta = uiInfo.m_margin;
                        break;
                    case UIInfo.Fit.WParentHSelf:
                        //width:Parentへアンカーする。 height:子要素のサイズに合わせる
                        rectTransform.anchorMin = Vector2.zero;
                        rectTransform.anchorMax = Vector2.one;
                        rectTransform.anchoredPosition = Vector2.zero;
                        rectTransform.sizeDelta = uiInfo.m_margin;
                        LayoutElement le_pow = ret.AddComponent<LayoutElement>();
                        le_pow.flexibleWidth = 1;
                        break;
                    case UIInfo.Fit.Self:
                        ContentSizeFitter csf = ret.AddComponent<ContentSizeFitter>();
                        csf.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;    //width/heightを中身（文字など）サイズに合わせる
                        csf.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
                        break;
                    case UIInfo.Fit.Fixed:
                    case UIInfo.Fit.Flexible:
                        RectTransform rt = ret.transform as RectTransform;
                        rt.localPosition = uiInfo.m_position;
                        rt.sizeDelta = uiInfo.m_uiSize;
                        LayoutElement le_fixed = ret.AddComponent<LayoutElement>();
                        if (uiInfo.m_uiSize.x != 0) le_fixed.minWidth = uiInfo.m_uiSize.x;
                        if (uiInfo.m_uiSize.y != 0) le_fixed.minHeight = uiInfo.m_uiSize.y;
                        if (uiInfo.m_flexWidth != 0) le_fixed.flexibleWidth = uiInfo.m_flexWidth;
                        if (uiInfo.m_flexHeight != 0) le_fixed.flexibleHeight = uiInfo.m_flexHeight;
                        break;
                    case UIInfo.Fit.WParentHFrexible:
                        rectTransform.anchorMin = Vector2.zero;
                        rectTransform.anchorMax = Vector2.one;
                        rectTransform.anchoredPosition = Vector2.zero;
                        rectTransform.sizeDelta = uiInfo.m_margin;
                        LayoutElement le_h = ret.AddComponent<LayoutElement>();
                        le_h.flexibleHeight = 1;
                        break;
                    case UIInfo.Fit.UnSpecified:
                    default: break; //pass
                }
            }
            ret.layer = LayerMask.NameToLayer(UI_LAYER_NAME);
            if (parent != null)
            {
                ret.transform.SetParent(parent.transform, false);
                ret.layer = parent.layer;
            }
            return ret;
        }

        public static EventSystem CreateEventSystem()
        {
            var esys = UnityEngine.Object.FindObjectOfType<EventSystem>();
            if (esys == null)
            {
                var eventSystem = new GameObject("EventSystem");
                esys = eventSystem.AddComponent<EventSystem>();
                eventSystem.AddComponent<StandaloneInputModule>();
            }
            return esys;
        }
        private static LayoutType getLayoutGroupTyep(GameObject go)
        {
            if (go == null) return LayoutType.None;
            LayoutGroup lg = go.GetComponent<LayoutGroup>();
            if (lg is VerticalLayoutGroup) return LayoutType.Vertical;
            else if (lg is HorizontalLayoutGroup) return LayoutType.Horizontal;
            else if (lg is GridLayoutGroup) return LayoutType.Grid;
            else return LayoutType.None;
        }


        private static void configSelectableColors(Selectable selectable)
        {
            ColorBlock colors = selectable.colors;
            colors.normalColor = COLOR_SELECTABLE_NORMAL;
            colors.highlightedColor = COLOR_SELECTABLE_HOVER;
            colors.pressedColor = COLOR_SELECTABLE_PRESSED;
            colors.selectedColor = COLOR_SELECTABLE_SELECTED;
            colors.disabledColor = COLOR_SELECTABLE_DISABLED;
            selectable.colors = colors;
        }

        private static LayoutGroup configLyaoutGroup(GameObject go, LayoutType layoutGroupType, int paddingPx = 5)
        { return configLyaoutGroup(go, layoutGroupType, paddingPx, paddingPx, paddingPx, paddingPx); }
        private static LayoutGroup configLyaoutGroup(GameObject go, LayoutType layoutGroupType, int padding_left, int padding_right, int padding_top, int padding_bottom)
        {
            switch (layoutGroupType)
            {
                case LayoutType.Grid:
                    GridLayoutGroup glg = go.AddComponent<GridLayoutGroup>();
                    glg.padding = new RectOffset(padding_left, padding_right, padding_top, padding_bottom);
                    glg.spacing = new Vector2(LAYOUTGROUP_SPACING, LAYOUTGROUP_SPACING);
                    return glg;
                case LayoutType.Horizontal:
                    HorizontalLayoutGroup hlg = go.AddComponent<HorizontalLayoutGroup>();
                    hlg.childControlWidth = true;   // false;
                    hlg.childControlHeight = true;
                    hlg.childScaleWidth = true;
                    hlg.childScaleHeight = false;
                    hlg.childForceExpandWidth = false;
                    hlg.childForceExpandHeight = false;
                    hlg.spacing = LAYOUTGROUP_SPACING;
                    hlg.padding = new RectOffset(padding_left, padding_right, padding_top, padding_bottom);
                    return hlg;
                case LayoutType.Vertical:
                    VerticalLayoutGroup vlg = go.AddComponent<VerticalLayoutGroup>();
                    vlg.childControlWidth = true;   // false;
                    vlg.childControlHeight = true;
                    vlg.childScaleWidth = true;
                    vlg.childScaleHeight = false;
                    vlg.childForceExpandWidth = false;
                    vlg.childForceExpandHeight = false;
                    vlg.spacing = LAYOUTGROUP_SPACING;
                    vlg.padding = new RectOffset(padding_left, padding_right, padding_top, padding_bottom);
                    return vlg;
                case LayoutType.None: break;
            }
            return null;
        }

        internal static T GetOrAddComponent<T>(this GameObject go) where T : Component
        {
            T ret = go.GetComponent<T>();
            return (ret != null) ? ret : go.AddComponent<T>();
        }

        public static Text addTextComponent(GameObject go, string label, UIInfo uiInfo)
        {
            if (uiInfo.m_textColor == default(Color)) uiInfo = uiInfo.Clone().textColor(COLOR_TEXT);
            Text lbl = go.AddComponent<Text>();
            lbl.text = label;
            lbl.color = uiInfo.m_textColor;
            lbl.font = TEXT_FONT;
            lbl.alignment = uiInfo.m_textAlignment;
            return lbl;
        }
        public static Image addImageComponent(GameObject go, Sprite sprite = null, UIInfo uiInfo = null)
        {
            if (uiInfo == null) uiInfo = new UIInfo().fit_Fixed();
            if (uiInfo.m_bgColor == default(Color)) { uiInfo.m_bgColor = COLOR_IMAGE_DEFAULT; }
            Image image = go.AddComponent<Image>();
            if (sprite != null)
            {
                image.sprite = sprite;
            }
            image.type = Image.Type.Simple;
            image.color = uiInfo.m_bgColor;
            return image;
        }

        public static Texture2D loadTexture(string path)
        {
            if (!File.Exists(path)) { Debug.Log(path + "が見つかりませんでした。"); return null; }
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(File.ReadAllBytes(path));
            return texture;
        }


        public static Canvas CreateCanvas(GameObject parent = null, RenderMode renderMode = RenderMode.ScreenSpaceOverlay, UIInfo uiInfo = null,
            Vector2 position = default(Vector2), Vector2 size = default(Vector2), string goName = "")
        {
            if (uiInfo == null) uiInfo = new UIInfo().fit_Fixed();
            if (uiInfo.is_fit_UnSpecified()) uiInfo = uiInfo.fit_Fixed();
            if (position == default(Vector2)) position = WINDOW_POSITION;
            if (size == default(Vector2)) size = WINDOW_SIZE;
            var goCanvas = CreateUIElement(goName == "" ? goname_Canvas.get() : goName, uiInfo, parent: parent); // new GameObject(goName == "" ? goname_Canvas.get() : goName);
            Canvas canvas = goCanvas.AddComponent<Canvas>();
            canvas.renderMode = renderMode;
            goCanvas.AddComponent<CanvasScaler>();
            goCanvas.AddComponent<GraphicRaycaster>();

            CreateEventSystem();

            if (goCanvas.transform.parent as RectTransform)
            {
                RectTransform rect = goCanvas.transform as RectTransform;
                rect.anchorMin = Vector2.zero;
                rect.anchorMax = Vector2.one;
                rect.anchoredPosition = Vector2.zero;
                rect.sizeDelta = Vector2.zero;
            }
            return canvas;
        }

        /// <summary>スクリーンにUIを描画する場合</summary>
        public static LayoutGroup CreateWindowWithCanvas_onScreen(Vector2 leftbottom = default(Vector2), Vector2 size = default(Vector2),
            UIInfo uiInfo = null, LayoutType layoutGroup = LayoutType.Vertical, bool draggable = true, float canvasScale = 1f,
            GameObject parent = null, string goName = "")
        {
            if (uiInfo == null) uiInfo = new UIInfo();
            if (leftbottom == default(Vector2)) leftbottom = WINDOW_POSITION;
            if (size == default(Vector2)) size = WINDOW_SIZE;
            Canvas canvas = CreateCanvas(parent, RenderMode.ScreenSpaceOverlay, new UIInfo().fit_Fixed(), leftbottom, size, goName);
            {
                CanvasScaler canvasScaler = canvas.gameObject.GetOrAddComponent<CanvasScaler>();
                canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
                canvasScaler.scaleFactor = canvasScale;
            }
            LayoutGroup container = CreatePanel(uiInfo.fit_Fixed(leftbottom, size), layoutGroup, canvas.gameObject, "container");
            {
                RectTransform rt = container.gameObject.GetOrAddComponent<RectTransform>();
                rt.anchorMin = Vector2.zero;            //anchorMin/Max 画面左下が原点。
                rt.anchorMax = Vector2.zero;
                rt.pivot = Vector2.zero;
                rt.sizeDelta = size;
                rt.anchoredPosition = leftbottom;
            }
            if (draggable) container.gameObject.GetOrAddComponent<DragBehaviour>();
            return container;
        }
        /// <summary>ゲーム空間にUIを描画する場合</summary>
        public static LayoutGroup CreateWindowWithCanvas_onWorld(Vector2 positionFromLeftBottom = default(Vector2), Vector2 size = default(Vector2),
            UIInfo uiInfo = null, LayoutType layoutGroup = LayoutType.Vertical, Camera camera = null, float meterPerPx = 0.001f, float windowScale = 1f,
            GameObject parent = null, string goName = "")
        {
            if (uiInfo == null) uiInfo = new UIInfo();
            if (positionFromLeftBottom == default(Vector2)) positionFromLeftBottom = WINDOW_POSITION;
            if (size == default(Vector2)) size = WINDOW_SIZE;
            Canvas canvas = CreateCanvas(parent, RenderMode.WorldSpace, new UIInfo().fit_Fixed(), positionFromLeftBottom, size, goName);
            {
                if (camera == null) camera = Camera.main;
                canvas.worldCamera = camera;
                canvas.gameObject.GetComponent<CanvasScaler>().dynamicPixelsPerUnit = 2;  //テキストに対する解像度。初期値1だとボケるため。
                //スケールが0.001なら、0.001m/pxとなり、Windowの幅が800pxならゲーム空間での幅が80cmになる。
                canvas.gameObject.GetComponent<RectTransform>().localScale = new Vector3(meterPerPx, meterPerPx, meterPerPx);
            }
            LayoutGroup container = CreatePanel(uiInfo.fit_Fixed(positionFromLeftBottom, size), layoutGroup, canvas.gameObject, "container");
            container.gameObject.GetComponent<RectTransform>().localScale = new Vector3(windowScale, windowScale, windowScale);
            return container;
        }

        public static LayoutGroup CreatePanel(UIInfo uiInfo = null, LayoutType layoutGroup = LayoutType.Vertical, GameObject parent = null, string goName = "")
        {
            if (uiInfo == null) uiInfo = new UIInfo().fit_Parent();
            if (uiInfo.is_fit_UnSpecified()) uiInfo = uiInfo.fit_Parent();
            GameObject panelGO = CreateUIElement(goName == "" ? goname_Panel.get() + "-" + layoutGroup.ToString() : goName, uiInfo, parent: parent);
            if (uiInfo.m_bgColor == default(Color)) uiInfo.m_bgColor = SWHelper.COLOR_AREA_BG;
            Image image = addImageComponent(panelGO, uiInfo: uiInfo);
            LayoutGroup lg = null;
            switch (layoutGroup)
            {
                case LayoutType.Horizontal: lg = configLyaoutGroup(panelGO, layoutGroup, 5, 5, 2, 2); break;
                case LayoutType.Vertical: lg = configLyaoutGroup(panelGO, layoutGroup, 2, 2, 5, 5); break;
                default: lg = configLyaoutGroup(panelGO, layoutGroup); break;
            }
            return lg;
        }

        /// <summary>HorizontalLayoutで左端・右端に配置したいとき、中間を埋めるためのUI要素。heightを指定することで縦方向の余白にも使える。</summary>
        public static Image CreateSpacer(GameObject parent, int height = 0, string goName = "Spacer")
        {

            Image ret = CreateImage(parent, uiInfo:
                new UIInfo().fit_Flexible(flexWidth: 1f, flexHeight: 0f).uiSize(new Vector2(0, height))
                , goName: goName);
            ret.color = Color.clear;
            return ret;
        }

        /// <summary>水平な区切り線</summary>
        public static Image CreateHorizontalBar(GameObject parent, int height = HR_HEIGHT, Color color = default(Color), string goName = "HorizontalBar")
        {
            if (color == default(Color)) color = Color.black;
            Image ret = CreateImage(parent, uiInfo:
                new UIInfo().fit_Flexible(flexWidth: 1f, flexHeight: 0f).uiSize(new Vector2(0, height))
                , goName: goName);
            ret.color = color;
            return ret;
        }

        /// <summary>
        /// - TetContainer  Component:Image, VerticalLayoutGroup
        ///   - Text        Component:Text
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="label"></param>
        /// <param name="uiInfo"></param>
        /// <param name="goName"></param>
        /// <returns></returns>
        public static Text CreateText(GameObject parent, string label, UIInfo uiInfo = null, string goName = "")
        {
            if (uiInfo == null) uiInfo = new UIInfo();
            if (uiInfo.is_fit_UnSpecified()) uiInfo = uiInfo.fit_WParentHSelf();
            if (uiInfo.m_bgColor == default(Color)) uiInfo.m_bgColor = COLOR_AREA_BG;
            Image image = CreateImage(parent, uiInfo: uiInfo.fit_WParentHSelf(), goName: goName == "" ? "TextContainer" : goName);
            GameObject container = image.gameObject;
            configLyaoutGroup(container, LayoutType.Vertical, paddingPx: 1);
            GameObject go = CreateUIElement(goname_Text.get(), uiInfo, parent: container);
            (go.transform as RectTransform).pivot = new Vector2(0f, 0.5f);  //拡縮・回転の基点、文字数が増えたときに右へサイズが膨らんでいくようにする。

            Text ret = addTextComponent(go, label, uiInfo);
            ret.fontSize = uiInfo.m_textSize;
            return ret;
        }

        /// <summary>
        /// - Button　　    Component:Button, Image（背景色用）, VerticalLayoutGroup
        ///   - buttonImage Component:Image　（ボタンの画像用）
        ///   - Text        Component:Text
        /// </summary>
        public static Button CreateButton(GameObject parent, UnityAction TaskOnClick, string labelStr = "", Sprite sprite = null, UIInfo uiInfo = null, string goName = "")
        {
            if (uiInfo == null) uiInfo = new UIInfo();
            if (uiInfo.is_fit_UnSpecified()) uiInfo = uiInfo.fit_WParentHSelf();
            Text text = CreateText(parent, labelStr, uiInfo, goName == "" ? goname_Button.get() : goName);
            text.alignment = TextAnchor.MiddleCenter;

            GameObject goButton = text.gameObject.getParent();  //CreateUIElement(goName == "" ? goname_Button.get() : goName, uiInfo, parent: parent);
            LayoutGroup lg = goButton.GetOrAddComponent<VerticalLayoutGroup>(); //CreateTextで作成したLayoutGroupを取得
            lg.childAlignment = TextAnchor.MiddleCenter;

            Image img = goButton.GetOrAddComponent<Image>();
            img.color = Color.white;  //背景色を白に変更、ホバーしたときの色変化を見やすくするため。


            if (sprite != null)  //ボタン画像がないときはGameObjectを追加しない。レイアウトが崩れないようにするため。
            {
                Image buttonImage = CreateImage(goButton, uiInfo: uiInfo, goName: "buttonImage");
                buttonImage.sprite = sprite;
                buttonImage.preserveAspect = true;  //引き伸ばしたときに画像のアスペクト比を保つ
                buttonImage.gameObject.transform.SetSiblingIndex(0);  //画像がテキストの上にくるように、GameObjectの順番を入れ替え
                buttonImage.color = new Color(1f, 1f, 1f, 0.5f);  //画像の色は変えずに半透明にする。
            }

            Button btn = goButton.AddComponent<Button>();
            configSelectableColors(btn);
            if (TaskOnClick != null) btn.onClick.AddListener(TaskOnClick);

            return btn;
        }
        public static Button CreateButton(GameObject parent, UnityAction TaskOnClick, string labelStr = "", Texture2D tex = null, UIInfo uiInfo = null, string goName = "")
        {
            if (tex == null) return CreateButton(parent, TaskOnClick: TaskOnClick, labelStr: labelStr, sprite: null, uiInfo: uiInfo, goName: goName);
            int width = tex.width;
            int height = tex.height;
            if (uiInfo != null && uiInfo.m_uiSize.x != 0f & uiInfo.m_uiSize.y != 0f)
            {
                width = (int)uiInfo.m_uiSize.x;
                height = (int)uiInfo.m_uiSize.y;
                tex = ResizeTexture(tex, width, height);
            }
            return CreateButton(parent, TaskOnClick, labelStr, Sprite.Create(tex, new Rect(0, 0, width, height), Vector2.zero), uiInfo, goName);
        }
        public static Button CreateButton(GameObject parent, string texPath, UnityAction TaskOnClick = null,
            string labelStr = "", UIInfo uiInfo = null, string goName = "")
        { return CreateButton(parent, TaskOnClick, labelStr, loadTexture(texPath), uiInfo, goName); }

        public static Image CreateImage(GameObject parent, Sprite sprite = null, UIInfo uiInfo = null, string goName = "")
        {
            if (uiInfo == null) uiInfo = new UIInfo();
            if (uiInfo.is_fit_UnSpecified()) uiInfo = uiInfo.fit_Self();
            GameObject go = CreateUIElement(goName == "" ? goname_Image.get() : goName, uiInfo, parent: parent);
            return addImageComponent(go, sprite, uiInfo);
        }
        public static Image CreateImage(GameObject parent, Texture2D tex, UIInfo uiInfo = null, string goName = "")
        { return CreateImage(parent, Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero), uiInfo, goName); }
        public static Image CreateImage(GameObject parent, string texturePath, int width = 0, int height = 0, UIInfo uiInfo = null, string goName = "")
        {
            Texture2D tex = loadTexture(texturePath);
            if (width != 0 & height != 0) tex = ResizeTexture(tex, width, height);
            return CreateImage(parent, tex, uiInfo, goName);
        }

        public static Texture2D ResizeTexture(Texture2D srcTexture, int newWidth, int newHeight)
        {
            var resizedTexture = new Texture2D(newWidth, newHeight);
            Graphics.ConvertTexture(srcTexture, resizedTexture);
            return resizedTexture;
        }

        public static Slider CreateSlider(GameObject parent, UnityAction<float> onValueChanged, string goName = "")
        {
            // Create GOs Hierarchy
            GameObject root = CreateUIElement(goName == "" ? goname_Slider.get() : goName, new UIInfo(), parent: parent);
            GameObject background = CreateUIElement("Background", new UIInfo(), parent: root);
            GameObject fillArea = CreateUIElement("Fill Area", new UIInfo(), parent: root);
            GameObject fill = CreateUIElement("Fill", new UIInfo(), parent: fillArea);
            GameObject handleArea = CreateUIElement("Handle Slide Area", new UIInfo(), parent: root);
            GameObject handle = CreateUIElement("Handle", new UIInfo(), parent: handleArea);

            // Background
            Image backgroundImage = addImageComponent(background, uiInfo: new UIInfo().fit_Parent().bgColor(COLOR_SLIDER_BACKGROUND));
            RectTransform backgroundRect = background.GetComponent<RectTransform>();
            backgroundRect.anchorMin = new Vector2(0, 0.25f);
            backgroundRect.anchorMax = new Vector2(1, 0.75f);
            backgroundRect.sizeDelta = new Vector2(0, 0);

            // Fill Area
            RectTransform fillAreaRect = fillArea.GetComponent<RectTransform>();
            fillAreaRect.anchorMin = new Vector2(0, 0.25f);
            fillAreaRect.anchorMax = new Vector2(1, 0.75f);
            fillAreaRect.anchoredPosition = new Vector2(-5, 0);
            fillAreaRect.sizeDelta = new Vector2(-20, 0);

            // Fill
            addImageComponent(fill, uiInfo: new UIInfo().fit_Parent().bgColor(COLOR_AREA_BG));

            RectTransform fillRect = fill.GetComponent<RectTransform>();
            fillRect.sizeDelta = new Vector2(10, 0);

            // Handle Area
            RectTransform handleAreaRect = handleArea.GetComponent<RectTransform>();
            handleAreaRect.sizeDelta = new Vector2(-20, 0);
            handleAreaRect.anchorMin = new Vector2(0, 0);
            handleAreaRect.anchorMax = new Vector2(1, 1);

            // Handle
            Image handleImage = addImageComponent(handle, uiInfo: new UIInfo().fit_Parent().bgColor(COLOR_SLIDER_HANDLE));

            RectTransform handleRect = handle.GetComponent<RectTransform>();
            handleRect.sizeDelta = new Vector2(20, 0);

            // Setup slider component
            LayoutElement sliderLe = root.GetOrAddComponent<LayoutElement>();
            sliderLe.flexibleWidth = 1f;
            sliderLe.minWidth = SLIDER_MIN_WIDTH; sliderLe.minHeight = SLIDER_MIN_HEIGHT;
            Slider slider = root.AddComponent<Slider>();
            slider.fillRect = fill.GetComponent<RectTransform>();
            slider.handleRect = handle.GetComponent<RectTransform>();
            slider.targetGraphic = handleImage;
            slider.direction = Slider.Direction.LeftToRight;
            configSelectableColors(slider);
            slider.onValueChanged.AddListener(onValueChanged);
            return slider;
        }



        public static Scrollbar CreateScrollbar(GameObject parent, UIInfo uiInfo = null, string goName = "")
        {
            if (uiInfo == null) uiInfo = new UIInfo().fit_Fixed(Vector2.zero, UIELEMENT_SIZE);
            if (uiInfo.is_fit_UnSpecified()) uiInfo = uiInfo.fit_Fixed().position(Vector2.zero).uiSize(UIELEMENT_SIZE);
            // Create GOs Hierarchy
            GameObject scrollbarRoot = CreateUIElement(goName == "" ? goname_ScrollBar.get() : goName, uiInfo, parent: parent);
            GameObject sliderArea = CreateUIElement("Sliding Area", new UIInfo(), parent: scrollbarRoot);
            GameObject handle = CreateUIElement("Handle", new UIInfo(), parent: sliderArea);


            Image bgImage = addImageComponent(scrollbarRoot, uiInfo: new UIInfo().fit_Parent().bgColor(COLOR_SLIDER_BACKGROUND));

            Image handleImage = addImageComponent(handle);

            RectTransform sliderAreaRect = sliderArea.GetComponent<RectTransform>();
            sliderAreaRect.sizeDelta = new Vector2(-20, -20);
            sliderAreaRect.anchorMin = Vector2.zero;
            sliderAreaRect.anchorMax = Vector2.one;

            RectTransform handleRect = handle.GetComponent<RectTransform>();
            handleRect.sizeDelta = new Vector2(20, 20);

            Scrollbar scrollbar = scrollbarRoot.AddComponent<Scrollbar>();
            scrollbar.handleRect = handleRect;
            scrollbar.targetGraphic = handleImage;
            configSelectableColors(scrollbar);
            return scrollbar;
        }

        public static Toggle CreateToggle(GameObject parent, UnityAction<bool> onValueChanged, string labelStr = "Toggle", bool isOn = true,
            UIInfo uiInfo = null, string goName = "")
        {
            if (uiInfo == null) uiInfo = new UIInfo().fit_Parent();
            if (uiInfo.is_fit_UnSpecified()) uiInfo = uiInfo.fit_Parent();
            // Set up hierarchy
            GameObject goToggleRoot = CreateUIElement(goName == "" ? goname_Toggle.get() : goName, uiInfo, parent: parent);
            LayoutGroup lg = configLyaoutGroup(goToggleRoot, LayoutType.Horizontal, paddingPx: 0);
            lg.childAlignment = TextAnchor.MiddleLeft;
            Image bgImage = CreateImage(parent: goToggleRoot,
                uiInfo: new UIInfo().fit_Flexible(0f, 0f, new Vector2(TOGGLE_BACKGROUND_SIZE, TOGGLE_BACKGROUND_SIZE)).bgColor(COLOR_SELECTABLE_ON), goName: "Background");
            GameObject goBackground = bgImage.gameObject;

            Image checkmarkImage = CreateImage(parent: goBackground,
              uiInfo: new UIInfo().fit_Fixed().bgColor(Color.white), goName: "Checkmark");
            GameObject goCheckmark = checkmarkImage.gameObject;
            RectTransform checkmarkRect = goCheckmark.GetComponent<RectTransform>();
            checkmarkRect.anchorMin = new Vector2(0.5f, 0.5f);
            checkmarkRect.anchorMax = new Vector2(0.5f, 0.5f);
            checkmarkRect.anchoredPosition = Vector2.zero;
            checkmarkRect.sizeDelta = new Vector2(TOGGLE_BACKGROUND_SIZE / 2, TOGGLE_BACKGROUND_SIZE / 2);

            Text label = CreateText(parent: goToggleRoot, uiInfo: new UIInfo().fit_Parent(), label: labelStr);

            Toggle toggle = goToggleRoot.AddComponent<Toggle>();
            toggle.isOn = isOn;
            toggle.graphic = checkmarkImage;
            toggle.targetGraphic = bgImage;
            configSelectableColors(toggle);
            toggle.onValueChanged.AddListener(onValueChanged);

            return toggle;
        }

        public static ToggleGroup CreateRadioButton<T>(GameObject parent, UnityAction<T> onValueChanged, Dictionary<string, T> showValueDict, int selected = 0,
            LayoutType layoutGroup = LayoutType.Horizontal, UIInfo uiInfo = null, string goName = "")
        {
            if (uiInfo == null) uiInfo = new UIInfo();
            if (uiInfo.is_fit_UnSpecified()) uiInfo = uiInfo.fit_WParentHSelf();
            GameObject goRadioPanel = CreatePanel(parent: parent, layoutGroup: layoutGroup, uiInfo: uiInfo, goName: goname_RadioButton.get()).gameObject;
            ToggleGroup toggleGroup = goRadioPanel.AddComponent<ToggleGroup>();
            int i = 0;
            foreach (var kvp in showValueDict)
            {
                string showStr = kvp.Key;
                T selectedValue = kvp.Value;
                Toggle t = CreateToggle(parent: goRadioPanel, (b) => { if (b) onValueChanged(selectedValue); }, labelStr: showStr, isOn: i == selected);
                t.group = toggleGroup;
                i++;
            }
            return toggleGroup;
        }

        public static InputField CreateInputField(GameObject parent, UnityAction<string> onEndEdit, UnityAction<string> onValueChanged,
            string defaultText = "", int lineCount = 1, UIInfo uiInfo = null, string goName = "")
        {
            if (uiInfo == null) uiInfo = new UIInfo();
            if (uiInfo.is_fit_UnSpecified()) uiInfo = uiInfo.fit_WParentHSelf();
            GameObject goInputField = CreateUIElement(goName == "" ? goname_InputField.get() : goName, uiInfo, parent: parent);
            GameObject goPlaceholder = CreateUIElement("Placeholder", new UIInfo(), parent: goInputField);
            GameObject goText = CreateUIElement("Text", new UIInfo(), parent: goInputField);

            Image image = addImageComponent(goInputField, uiInfo: new UIInfo().fit_Parent().bgColor(COLOR_SELECTABLE));

            InputField inputField = goInputField.AddComponent<InputField>();
            configSelectableColors(inputField);
            LayoutElement leInputField = goInputField.GetOrAddComponent<LayoutElement>();
            leInputField.minWidth = INPUTFIELD_MIN_SIZE.x;
            leInputField.minHeight = INPUTFIELD_MIN_SIZE.y;

            Text text = addTextComponent(goText, defaultText, uiInfo);
            text.supportRichText = false;

            Color placeholderColor = text.color;
            placeholderColor.a *= 0.5f;
            Text placeholder = addTextComponent(goPlaceholder, "Enter text...", uiInfo: new UIInfo().textColor(placeholderColor));

            if (lineCount >= 2)  //複数行の入力の場合の設定
            {
                inputField.lineType = InputField.LineType.MultiLineNewline;     //垂直にスクロールし、オーバーフローし、リターンキーで改行文字を挿入する複数行の InputField です。
                leInputField.minHeight = INPUTFIELD_MIN_SIZE.y * lineCount;
                text.alignment = TextAnchor.UpperLeft;
                placeholder.alignment = TextAnchor.UpperLeft;
            }

            RectTransform textRectTransform = goText.GetComponent<RectTransform>();
            textRectTransform.anchorMin = Vector2.zero;
            textRectTransform.anchorMax = Vector2.one;
            textRectTransform.sizeDelta = Vector2.zero;
            textRectTransform.offsetMin = new Vector2(10, 6);
            textRectTransform.offsetMax = new Vector2(-10, -7);

            RectTransform placeholderRectTransform = goPlaceholder.GetComponent<RectTransform>();
            placeholderRectTransform.anchorMin = Vector2.zero;
            placeholderRectTransform.anchorMax = Vector2.one;
            placeholderRectTransform.sizeDelta = Vector2.zero;
            placeholderRectTransform.offsetMin = new Vector2(10, 6);
            placeholderRectTransform.offsetMax = new Vector2(-10, -7);

            inputField.textComponent = text;
            inputField.placeholder = placeholder;
            if (onValueChanged != null) inputField.onValueChanged.AddListener(onValueChanged);
            if (onEndEdit != null) inputField.onEndEdit.AddListener(onEndEdit);

            return inputField;
        }


        /// <returns>contentのGameObject,ScrollViewに要素を追加するときはこのGameObjectに追加すること。</returns>
        public static LayoutGroup CreateScrollView(GameObject parent, LayoutType contentPanelLayoutGroupType = LayoutType.Vertical,
            float scrollSensitivity = 20, UIInfo uiInfo = null, string goName = "")
        {
            if (uiInfo == null) uiInfo = new UIInfo().fit_Parent();
            if (uiInfo.is_fit_UnSpecified()) uiInfo = uiInfo.fit_Parent();
            if (uiInfo.m_bgColor == default(Color)) uiInfo = uiInfo.Clone().bgColor(COLOR_AREA_BG);
            GameObject goScrollView = CreateUIElement(goName == "" ? goname_ScrollView.get() : goName, uiInfo, parent: parent);
            GameObject goViewport = CreateUIElement("Viewport", new UIInfo().fit_Parent(), parent: goScrollView);
            GameObject goContent = CreateUIElement("Content", new UIInfo().fit_Self(), parent: goViewport);

            GameObject hScrollbar = CreateScrollbar(parent: goScrollView).gameObject;
            hScrollbar.name = "ScrollbarHorizontal";
            RectTransform hScrollbarRT = hScrollbar.GetComponent<RectTransform>();
            hScrollbarRT.anchorMin = Vector2.zero;
            hScrollbarRT.anchorMax = Vector2.right;
            hScrollbarRT.pivot = Vector2.zero;
            hScrollbarRT.sizeDelta = new Vector2(0, hScrollbarRT.sizeDelta.y);

            GameObject vScrollbar = CreateScrollbar(parent: goScrollView).gameObject;
            vScrollbar.name = "Scrollbar Vertical";
            vScrollbar.GetComponent<Scrollbar>().SetDirection(Scrollbar.Direction.BottomToTop, true);
            RectTransform vScrollbarRT = vScrollbar.GetComponent<RectTransform>();
            vScrollbarRT.anchorMin = Vector2.right;
            vScrollbarRT.anchorMax = Vector2.one;
            vScrollbarRT.pivot = Vector2.one;
            vScrollbarRT.sizeDelta = new Vector2(vScrollbarRT.sizeDelta.x, 0);

            RectTransform viewportRT = goViewport.GetComponent<RectTransform>();
            viewportRT.pivot = Vector2.up;

            RectTransform contentRT = goContent.GetComponent<RectTransform>();
            contentRT.anchorMin = Vector2.up;
            contentRT.anchorMax = Vector2.one;
            contentRT.sizeDelta = new Vector2(0, 300);
            contentRT.pivot = Vector2.up;

            ScrollRect scrollRect = goScrollView.AddComponent<ScrollRect>();
            scrollRect.content = contentRT;
            scrollRect.viewport = viewportRT;
            scrollRect.horizontalScrollbar = hScrollbar.GetComponent<Scrollbar>();
            scrollRect.verticalScrollbar = vScrollbar.GetComponent<Scrollbar>();
            scrollRect.horizontalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHideAndExpandViewport;
            scrollRect.verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHideAndExpandViewport;
            scrollRect.horizontalScrollbarSpacing = -3;
            scrollRect.verticalScrollbarSpacing = -3;
            scrollRect.scrollSensitivity = scrollSensitivity;
            Image rootImage = addImageComponent(goScrollView, uiInfo: uiInfo);

            Mask viewportMask = goViewport.AddComponent<Mask>();
            viewportMask.showMaskGraphic = false;

            Image viewportImage = addImageComponent(goViewport, uiInfo: new UIInfo().bgColor(COLOR_SCROLLVIEW_MASK));

            LayoutGroup lgContent = configLyaoutGroup(goContent, contentPanelLayoutGroupType);
            LayoutElement le_Content = GetOrAddComponent<LayoutElement>(goContent);
            le_Content.preferredWidth = GetOrAddComponent<RectTransform>(parent).sizeDelta.x - (SLIDER_MIN_WIDTH + 5);

            return lgContent;
        }

        /// <summary>親GameObjectを取得</summary>
        internal static GameObject getParent(this GameObject childObject) { return childObject.transform.parent.gameObject; }
        public static List<GameObject> foreach_GameObjectChildren(GameObject go, UnityAction<GameObject> unityAction = null)
        {
            List<GameObject> ret = new List<GameObject>();
            for (int i = 0; i < go.transform.childCount; ++i)
            {
                GameObject tgt = go.transform.GetChild(i).gameObject;
                if (unityAction != null) unityAction(tgt);
                ret.Add(tgt);
            }
            return ret;
        }



        class UniqueName
        {
            private long _count = 0;
            private readonly string Text;
            internal UniqueName(string str) { this.Text = str; }
            internal string get() { _count++; return Text + _count; }
        }
    }

}
